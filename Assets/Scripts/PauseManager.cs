﻿using UnityEngine;
using System.Collections;

public class PauseManager : MonoBehaviour {

	private GameLogic game;
	public GameObject pausePanel;

	void Awake () {
		game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();
	}

	public void setPause(bool pause)
	{
		if(pause)
		{
			game.state = GameLogic.GameState.PAUSE;
		}
		else
		{
			game.state = GameLogic.GameState.PLAY;
		}
	}

	public void ReturnToMainScreen()
	{
		Application.LoadLevel( (int) Scenes.SceneList.Title);
	}

	public void goToNextChallenge()
	{
		//TODO: add the level change to the level chooser
		Application.LoadLevel( (int) Scenes.SceneList.GameScene);
	}

	public void retryLevel()
	{
		Application.LoadLevel( (int) Scenes.SceneList.GameScene);
	}

}
