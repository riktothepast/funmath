﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SendValue : MonoBehaviour {

    Text text;
    GameLogic GameLogic;
    public bool onlyOneUse;
    void Awake()
    {
        text = GetComponentInChildren<Text>();
        GameLogic = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();
    }

    public void SendValueToMathParser()
    {
        if (Time.timeScale <= 0)
            return;

        GameLogic.AddToOperationStack(text.text);
        if (onlyOneUse)
        {
            GetComponent<Button>().interactable = false;
            GetComponent<Animator>().Play("Hide");
        }
    }

    public void RestoreUsage(string value)
    {
        text.text = value;
        GetComponent<Button>().interactable = true;
        GetComponent<Animator>().Play("Show");
    }

    public void RestoreUsage()
    {
        GetComponent<Button>().interactable = true;
        GetComponent<Animator>().Play("Show");
    }

    public void ClearOperation()
    {
        GameLogic.ClearOperation();
    }
}
