﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {

	void Awake () {
	
	}


    /// <summary>Changes the scene time scale 
    /// <para>0 turns the pause, 1 resumes.</para>
    /// </summary> 
    public void TogglePause() {
        Debug.Log(Time.timeScale);
        if (Time.timeScale == 0)
            Time.timeScale = 1;
        else if (Time.timeScale == 1)
            Time.timeScale = 0;
    }
}
