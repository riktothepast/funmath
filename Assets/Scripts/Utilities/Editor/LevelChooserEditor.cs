﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

	[CustomEditor(typeof(LevelChooser))]
	public class LevelChooserExtension : Editor {
		
		public List<string> levelList = new List<string> ();
		public float test;
		int levelNumber = 0;
		
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector ();
			GUILayout.Label ("Load Level");
			
			levelNumber = EditorGUILayout.Popup (levelNumber, levelList.ToArray());
			
			if (GUILayout.Button ("Show Level List")) 
			{
				showList();
			}
			
			if (GUILayout.Button ("Load")) 
			{
				var levels = GameObject.FindGameObjectsWithTag("GameController");
				foreach(GameObject level in levels)
				{
					DestroyImmediate(level);
				}
				var selectedLevel = Resources.Load("Levels/"+ levelList[levelNumber]) as GameObject;
				PrefabUtility.InstantiatePrefab(selectedLevel);
			}
			
			var chooser = target as LevelChooser;
			if(levelList.Count > 0)
			{
				chooser.selectedLevel = levelList[levelNumber];
			}
		}
		
		void showList()
		{
			string[] fis = Directory.GetFiles(Application.dataPath + "/Resources/Levels", "*.prefab", SearchOption.AllDirectories);
			string[] separators = new string[2]{".","/Levels/"};
			Debug.Log ("list "+ fis.Length);
			levelList.Clear ();
			foreach( string item in fis)
			{
				string[] names = item.Split(separators, System.StringSplitOptions.None);
				levelList.Add(names [1]);
			}
			
		}
}

