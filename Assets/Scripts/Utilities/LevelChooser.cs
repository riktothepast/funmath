﻿using UnityEngine;
using System.Collections;

public class LevelChooser : MonoBehaviour {


	[HideInInspector]
	public LevelChooser instance;

	public string selectedLevel;

	void Awake () {
			
		if (instance) 
		{
			Destroy(gameObject);
			Debug.LogWarning("There is already a "+ gameObject.name +" instantiated");
		}
		else 
		{
			DontDestroyOnLoad(gameObject);
			instance = GetComponent<LevelChooser>();
		}
			
	}

}
