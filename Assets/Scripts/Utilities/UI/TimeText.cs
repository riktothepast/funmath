﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeText : MonoBehaviour {

	Text timerText;
	GameLogic game;

	// Use this for initialization
	void Awake () {
		game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();
		timerText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(game.state == GameLogic.GameState.PLAY)
		{
			timerText.text = convertToTime(game.timeLeft);
		}
	}

	string convertToTime(float timeToConvert)
	{

		int minutes = Mathf.FloorToInt(timeToConvert / 60F);
		int seconds = Mathf.FloorToInt(timeToConvert - minutes * 60);
		
		string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);
		return niceTime;
	}
}
