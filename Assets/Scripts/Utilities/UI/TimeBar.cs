﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeBar : MonoBehaviour {

	GameLogic game;
	Slider timeSlider;

	// Use this for initialization
	void Awake () {
		game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();
		timeSlider = GetComponent<Slider>();
        timeSlider.value = 1;
	}
	
	void Update () {

		if(game.state == GameLogic.GameState.PLAY)
		{
			if(game.timeLeft > 0 && game.timeLeft < game.startTime)
				timeSlider.value = 	game.timeLeft / game.startTime;
		}
	}
}
