﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerMeterScript : MonoBehaviour {
    Slider powerSlider;
	// Use this for initialization
	void Start () {
        powerSlider = GetComponent<Slider>();
        powerSlider.value = 0;
	}
	
    public void AddToPowerMeter(float resolutionTime, float barTime, float graceBonusTime)
    {
        powerSlider.value += (resolutionTime + graceBonusTime) / barTime;

        if (powerSlider.value >= powerSlider.maxValue) {
            powerSlider.value = powerSlider.minValue;
        }
    }
}
