﻿using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using System.Collections;

public class ChangeLanguage : MonoBehaviour {
    LanguageManager manager;

	// Use this for initialization
	void Start () {
        manager = LanguageManager.Instance;
	}

    public void SetNewLanguage(string languageName)
    {
        PlayerPrefs.SetString("Language", languageName);
        PlayerPrefs.Save();
        manager.ChangeLanguage(PlayerPrefs.GetString("Language", languageName));
        LanguageManager.Instance.ChangeLanguage(languageName);
    }
}
