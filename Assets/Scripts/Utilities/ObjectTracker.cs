﻿using UnityEngine;
using System.Collections;

public class ObjectTracker : MonoBehaviour {
    public Transform trans;
    public Vector3 separation;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (trans != null)
        {
            Vector3 position = new Vector3(trans.position.x + separation.x, trans.position.y + separation.y, 0);
            transform.position = position;
        }
	}
}
