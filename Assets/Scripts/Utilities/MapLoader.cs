﻿using UnityEngine;
using System.Collections;

public class MapLoader : MonoBehaviour {

	void Awake () {
		var levels = GameObject.FindGameObjectsWithTag("GameController");
		foreach(GameObject level in levels)
		{
			Destroy(level);
		}
		var chooser = GameObject.FindGameObjectWithTag("LevelChooser").GetComponent<LevelChooser>();
		var selectedLevel = Resources.Load("Levels/"+chooser.selectedLevel) as GameObject;
		Instantiate  (selectedLevel);
	}
}
