﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckUnlockLevel : MonoBehaviour {

	public Levels.LevelList levelName;

	public bool isAlwaysUnlocked;

	public Sprite lockImage, unlockImage;

	// Use this for initialization
	void Awake () {
		var unlockStatus = PlayerPrefs.GetInt(levelName.ToString()+"Unlocked");
		if(unlockStatus == 1 || isAlwaysUnlocked)
		{
			GetComponent<Image>().sprite = unlockImage;
		}else if(unlockStatus == 0)
		{
			GetComponent<Image>().sprite = lockImage;
			GetComponent<Button>().interactable = false;
		}
	}

	public void setLevel()
	{
		GameObject.FindGameObjectWithTag("LevelChooser").GetComponent<LevelChooser>().selectedLevel = levelName.ToString();
		Application.LoadLevel((int) Scenes.SceneList.GameScene);
		//TODO: select the level to play
	}

}
