﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollingScript : MonoBehaviour {
    public Vector2 scrollFactor;
    float mapWidth, mapHeight;
    Sprite sprite;
    public GameObject terrain;
    Sprite GOSprite;
	// Use this for initialization
	void Start () {
        sprite = GetComponent<SpriteRenderer>().sprite;
        GOSprite = terrain.GetComponent<SpriteRenderer>().sprite;
        StartCoroutine(DoParallax());   
	}

    IEnumerator DoParallax()
    {
        while (true)
        {
            transform.localPosition += Vector3.right * Time.deltaTime * scrollFactor.x;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    void Update()
    {

        if (scrollFactor.x > 0)
        {
            if (transform.localPosition.x - (sprite.textureRect.width / sprite.pixelsPerUnit) > terrain.transform.localPosition.x + (GOSprite.textureRect.width / GOSprite.pixelsPerUnit) /2)
            {
                transform.localPosition = new Vector3(terrain.transform.localPosition.x - GOSprite.textureRect.width / GOSprite.pixelsPerUnit - (sprite.textureRect.width / sprite.pixelsPerUnit)/2, transform.localPosition.y, transform.localPosition.z);
            }
        }
    }
}
