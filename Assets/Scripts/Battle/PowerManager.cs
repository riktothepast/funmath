﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerManager : MonoBehaviour {
    
	public Slider powerMeter;
    Slider slider;
    public bool canAttack;


	void Awake () {

        slider = (Slider)Instantiate(powerMeter, GameObject.FindGameObjectWithTag("TopCamera").GetComponent<Camera>().WorldToScreenPoint(transform.position), transform.rotation);
        slider.transform.SetParent(GameObject.Find("BattleUI").transform);
        slider.GetComponent<ObjectTracker>().trans = this.gameObject.transform;
        slider.GetComponent<ObjectTracker>().separation = new Vector3(0, 2, 0);
        slider.transform.localScale = new Vector3(1,1,1);
        GetComponent<EnergyScript>().ReferenceText(slider.transform.GetChild(2).GetComponent<Text>());
	}


    public void AddToPowerMeter(float resolutionTime, float barTime, float graceBonusTime)
    {
        Debug.Log("the value is " + resolutionTime );
        slider.value += resolutionTime  / barTime;

        if (slider.value >= slider.maxValue)
        {
            slider.value = slider.minValue;
            canAttack = true;
        }
    }

    public void AddToPowerMeter(float value)
    {
        slider.value += value;

        if (slider.value >= slider.maxValue)
        {
            slider.value = slider.minValue;
            canAttack = true;
        }
    }
}
