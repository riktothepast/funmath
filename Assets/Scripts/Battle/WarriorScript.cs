﻿using UnityEngine;
using System.Collections;

public class WarriorScript : MonoBehaviour {
    PowerManager powerManager;
    public int attackPower;
    public float currentExpPoints;
    public int currentLevel;
	private GameLogic game;


	void Start () {
        powerManager = GetComponent<PowerManager>();
		game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();

	}
	
	void Update () {
		if(game.state == GameLogic.GameState.PLAY)
		{
			if (powerManager.canAttack)
			{
				powerManager.canAttack = false;
				Attack();
			}
		}
	}

    void Attack()
    {
        GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnergyScript>().SubtractPoints(attackPower);
        GetComponent<Animator>().Play("PlayerAttack");
    }

    public void CheckForLevelUpgrade()
    {
        float neededXPPoints = 0;
        if (currentExpPoints >= neededXPPoints)
        {
            if(currentLevel<99)
                currentLevel++;

            attackPower += (int)Mathf.Ceil(attackPower * 0.15f);
            //PlayerPrefs.SetInt("playerName_attackPower", attackPower);
            //PlayerPrefs.SetInt("playerName_level", currentLevel);
        }
    }

    //we will store these levels of player prefs?
    public float GetValueForNewLevel(float previousXPLVL)
    { 
        return previousXPLVL + previousXPLVL * 0.10f;
    }

}
