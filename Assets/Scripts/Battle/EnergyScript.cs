﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnergyScript : MonoBehaviour {
    public int EnergyPoints;
    int initialEnergy;
    Text healthInfo;
	GameLogic game;

	// Use this for initialization
	void Awake () {
        initialEnergy = EnergyPoints;
		game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();
	}

    public void ReferenceText(Text text)
    {
        healthInfo = text;
        initialEnergy = EnergyPoints;
        healthInfo.text = EnergyPoints + " / " + initialEnergy;
    }
	
	// Update is called once per frame
	void Update () {
		if(EnergyPoints <= 0)
		{
			if(gameObject.CompareTag("Player"))
			{
				game.setLevelLost();
			}else if(gameObject.CompareTag("Enemy"))
			{
				game.setLevelWon();
			}
		}
	}

    public void AddPoints(int val)
    {
        EnergyPoints += val;
        healthInfo.text = EnergyPoints + " / " + initialEnergy;
    }

    public void SubtractPoints(int val)
    {
        EnergyPoints -= val;
        healthInfo.text = EnergyPoints + " / " + initialEnergy;
        Debug.Log(transform.tag + " " + EnergyPoints);
    }
}
