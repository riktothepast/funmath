﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
    public float barUpdateTime;
    public float barAmountToAdd;
    PowerManager powerManager;
    public int attackPower;
	private GameLogic game;


	void Awake () {
		game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>();
        powerManager = GetComponent<PowerManager>();
        InvokeRepeating("AddToBar", barUpdateTime, barUpdateTime);
	}
	
	void Update () {
		if(game.state == GameLogic.GameState.PLAY)
		{
			if (powerManager.canAttack) {
				powerManager.canAttack = false;
				Attack();
			}
		}
	}

    void AddToBar()
    {
		if(game.state == GameLogic.GameState.PLAY)
		{
			powerManager.AddToPowerMeter(barAmountToAdd);
		}
    }

    void Attack()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<EnergyScript>().SubtractPoints(attackPower);
        GetComponent<Animator>().Play("EnemyAttack");
    }
}
