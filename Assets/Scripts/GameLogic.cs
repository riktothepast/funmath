﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;
using System.Collections.Generic;

public class GameLogic : MonoBehaviour {

	public enum Operators{
		Subtraction,
		Addition,
		Multiplication,
		Division
	}

	public enum GameState
	{
		INTRO,
		PLAY,
		PAUSE,
		OVER
	}

	[HideInInspector]
	public GameState state;

	public bool UnlocksLevel;
	public Levels.LevelList levelName;
	public Levels.LevelList levelToUnlock;

	public List<Operators> availableOperators;

    public float timeLeft = 30f;
    public GameObject node;
	   

	[HideInInspector]
	public float startTime;

    public bool hasMandatory;
    bool usingMandatory;
    bool numericMandatory;
    public int mandatoryValue;
    public Vector2 numberRange;
    public Vector2 divisonRange;

	[Range(2,4)]
	public int amountToAnswer;

	int requestedValue = 0;

	int result;
	int currentOperationValue;
	string lastOperator = null;
	List<int> nodeValues = new List<int>();
    List<int> reserved = new List<int>();
    int currentAmount;
    Text requestDisplay;
    Text yourAnswerDisplay;
    Text playerOperation;

	private GameObject wonCanvas, lostCanvas;


	void Awake () {
        LanguageManager.Instance.ChangeLanguage(PlayerPrefs.GetString("Language", "en"));
		wonCanvas = GameObject.Find("WonCanvas");
		wonCanvas.SetActive(false);
		lostCanvas = GameObject.Find("LostCanvas");
		lostCanvas.SetActive(false);
		startTime = timeLeft;
		state = GameState.INTRO;
		foreach(Operators op in availableOperators)
		{
			if(op == Operators.Addition)
			{
				GameObject.Find("AdditionButton").GetComponent<Button>().interactable = true;
			}
            if (op == Operators.Subtraction)
			{
				GameObject.Find("SubtractionButton").GetComponent<Button>().interactable = true;				
			}
			if(op == Operators.Multiplication)
			{
				GameObject.Find("MultiplicationButton").GetComponent<Button>().interactable = true;
			}
			if(op == Operators.Division)
			{
				GameObject.Find("DivisionButton").GetComponent<Button>().interactable = true;
			}
		}
        requestDisplay = GameObject.FindGameObjectWithTag("RequestedValue").GetComponent<Text>();
        yourAnswerDisplay = GameObject.Find("YourAnswer").GetComponent<Text>();
        playerOperation = GameObject.Find("PlayerOperation").GetComponent<Text>();
	}

    public void GenerateNewOperation()
    {
        usingMandatory = numericMandatory = hasMandatory;

        reserved.Clear();
        nodeValues.Clear();
        playerOperation.text = "";
        yourAnswerDisplay.text = "";
        Debug.Log("Generation new operation");
        timeLeft = startTime;

        nodeValues.Add(GetRandomNumber());
        nodeValues.Add(GetRandomNumber());
        nodeValues.Add(GetRandomNumber());
        nodeValues.Add(GetRandomNumber());
        nodeValues.Sort();

        //for (int x = 0; x < nodeValues.Count; x++)
        //    Debug.Log(nodeValues[x]);

        requestedValue = Generate();

        requestDisplay.text = LanguageManager.Instance.GetTextValue("GamePage.RequerimentMessage1") + " " + requestedValue + " " + LanguageManager.Instance.GetTextValue("GamePage.RequerimentMessage2") + " " + amountToAnswer + " " + LanguageManager.Instance.GetTextValue("GamePage.RequerimentMessage3");

        GameObject.FindGameObjectWithTag("Operable1").GetComponentInChildren<SendValue>().RestoreUsage(nodeValues[0].ToString());
        GameObject.FindGameObjectWithTag("Operable2").GetComponentInChildren<SendValue>().RestoreUsage(nodeValues[1].ToString());
        GameObject.FindGameObjectWithTag("Operable3").GetComponentInChildren<SendValue>().RestoreUsage(nodeValues[2].ToString());
        GameObject.FindGameObjectWithTag("Operable4").GetComponentInChildren<SendValue>().RestoreUsage(nodeValues[3].ToString());
    }
   
    public int GetRandomNumber()
    {
        int value = 0;

        if (hasMandatory && usingMandatory)
        {
            usingMandatory = false;
            value = mandatoryValue;
        }
        else
        {
            value = (int)Random.Range(numberRange.x, numberRange.y);
        }

        for (int x = 0; x < reserved.Count; x++)
            if (value == reserved[x])
                return GetRandomNumber();

        reserved.Add(value);
            return value;
    }

    public List<int> getDivisors(int value)
    {
        List<int> divisors = new List<int>();

        for (int x = 1; x < value; x++)
        {
            if (value % x == 0)
            {
                divisors.Add(x);
                Debug.Log("exact divisor is: " + x);
            }
        }
        return divisors;
    }

    int Generate()
    {
        List<int> usableValues = new List<int>();
        for (int x = 0; x < nodeValues.Count; x++)
        {
            usableValues.Add(nodeValues[x]);
        }
        yourAnswerDisplay.text = "";
        currentAmount = amountToAnswer;
        int returnValue = 0;
        switch (amountToAnswer)
        { 
            case 2:
                returnValue = GetOperationBetweenTwo(RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)), RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)));
                break;
            case 3:
                int temporal = GetOperationBetweenTwo(RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)), RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)));
                returnValue = GetOperationBetweenTwo(temporal, RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)));
                break;
            case 4:
                int temporal1 = GetOperationBetweenTwo(RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)), RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)));
                int temporal2 = GetOperationBetweenTwo(RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)), RemoveAndGetItem(usableValues, Random.Range(0, usableValues.Count)));
                returnValue = GetOperationBetweenTwo(temporal1, temporal2);                
                break;
            default:

                break;
        }
        return returnValue;
    }

    int GetOperationBetweenTwo(int n1, int n2) 
    {
        int returnValue = 0; 
        switch (availableOperators[Random.Range(0, availableOperators.Count)])
        { 
            case Operators.Addition:
                returnValue = n1 + n2;
               break;
            case Operators.Subtraction:
                if (n1 > n2)
                    returnValue = n1 - n2;
                if (n2 > n1)
                    returnValue = n2 - n1;
                break;
            case Operators.Multiplication:
                    returnValue = n1 * n2;
                break;
            case Operators.Division:
                if (amountToAnswer > 2)
                    return GetOperationBetweenTwo(n1, n2);
                returnValue = CheckIfNumbersAreDivisible(n1, n2);
                break;
        }
        return returnValue;
    }

    private int CheckIfNumbersAreDivisible(int n1, int n2)
	{
        int returnValue;
        nodeValues.Clear();
        int val1 = (n1 * (int)Random.Range(divisonRange.x, divisonRange.y));
        int val2 = (n2 * (int)Random.Range(divisonRange.x, divisonRange.y));

        List<int> num1 = getDivisors(val1);
        if(num1.Count > 1)
            num1.RemoveAt(0);
        List<int> num2 = getDivisors(val2);
        if (num2.Count > 1)
            num2.RemoveAt(0);
        int whichToUse = Random.Range(0, 1);
        if (whichToUse == 0)
        {
            int divisionValue = RemoveAndGetItem(num1, Random.Range(0, num1.Count));
            returnValue = val1 / divisionValue;
            nodeValues.Add(divisionValue);
            nodeValues.Add(val1);
            nodeValues.Add(divisionValue + Random.Range(-divisionValue + 2, 2));
        }
        else
        {
            int divisionValue = RemoveAndGetItem(num2, Random.Range(0, num2.Count));
            returnValue = val2 / divisionValue;
            nodeValues.Add(divisionValue);
            nodeValues.Add(val2);
            nodeValues.Add(divisionValue + Random.Range(-divisionValue + 2, 2));
        }

        nodeValues.Add(returnValue);
        nodeValues.Sort();
        return returnValue;
	}

    public void StartGame()
    {
        if (state == GameState.PLAY)
            return;
		state = GameState.PLAY;
       GenerateNewOperation();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == GameState.PLAY)
        {
            UpdateTime();

            // check for operation solved.
        }
    }

    // time manager?
    void UpdateTime()
    {
        if(timeLeft>0)
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            timeLeft = 0;

            Debug.Log("No time Left!");
        }
    }


	public void AddToOperationStack(string c) {
		playerOperation.text = playerOperation.text +" "+c.ToString();
		
		if (int.TryParse(c, out result)) {
			if (lastOperator == null)
			{
				currentOperationValue = int.Parse(c.ToString());
				currentAmount--;
			}
			else {
				switch (lastOperator)
				{ 
				case "+":
					currentOperationValue += int.Parse(c.ToString());
					currentAmount--;
					lastOperator = null;
					break;
				case "-":
					currentOperationValue -= int.Parse(c.ToString());
					currentAmount--;
					lastOperator = null;
					break;
				case "x":
					currentOperationValue *= int.Parse(c.ToString());
					currentAmount--;
					lastOperator = null;
					break;
				case "/":
                    currentOperationValue /= int.Parse(c.ToString());
					currentAmount--;
					lastOperator = null;
					break;
				}
			}
			
		}
		else if (!int.TryParse(c, out result))
		{
			lastOperator = c;
		}
		yourAnswerDisplay.text = currentOperationValue.ToString();
		
		if (currentAmount == 0)
		{
			if (currentOperationValue == requestedValue)
			{
				requestDisplay.text = "sucess.";
				Invoke("GenerateNewOperation", 0.5f);
				GameObject.FindGameObjectWithTag("Player").GetComponent<PowerManager>().AddToPowerMeter(timeLeft, startTime, 0.5f);
			}
			else {
				requestDisplay.text = "failed.";
				Invoke("GenerateNewOperation", 0.5f);
				GameObject.FindGameObjectWithTag("Enemy").GetComponent<PowerManager>().AddToPowerMeter(timeLeft, startTime, 0.5f);
				
			}
		}
	}

    public void ClearOperation()
    { 
        currentAmount = amountToAnswer;
        playerOperation.text = "";
        yourAnswerDisplay.text = "";
        currentOperationValue = 0;
        lastOperator = null;

        GameObject.FindGameObjectWithTag("Operable1").GetComponentInChildren<SendValue>().RestoreUsage();
        GameObject.FindGameObjectWithTag("Operable2").GetComponentInChildren<SendValue>().RestoreUsage();
        GameObject.FindGameObjectWithTag("Operable3").GetComponentInChildren<SendValue>().RestoreUsage();
        GameObject.FindGameObjectWithTag("Operable4").GetComponentInChildren<SendValue>().RestoreUsage();
    }


	public void setLevelWon()
	{
		state = GameState.OVER;
		if(UnlocksLevel && PlayerPrefs.GetInt(levelToUnlock.ToString()+"Unlocked") == 0 )
		{
			PlayerPrefs.SetInt(levelToUnlock.ToString()+"Unlocked",1);
		}
		wonCanvas.SetActive(true);
	}

	public void setLevelLost()
	{
		state = GameState.OVER;
		lostCanvas.SetActive(true);
	}

    public int RemoveAndGetItem(List<int> list, int iIndexToRemove)
    {
        if (numericMandatory)
        {
            iIndexToRemove = list.IndexOf(mandatoryValue);
            numericMandatory = false;
        }
        if (iIndexToRemove > list.Count)
            iIndexToRemove = 0;

        Debug.Log(iIndexToRemove + " " + list.Count);

        int item = list[iIndexToRemove];
        list.RemoveAt(iIndexToRemove);
        return item;
    }

}