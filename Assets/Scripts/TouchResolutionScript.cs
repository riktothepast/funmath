﻿using UnityEngine;
using System.Collections;

public class TouchResolutionScript : MonoBehaviour {
    bool move = false;
    TKSwipeRecognizer swipeRecognizer;
	// Use this for initialization
    void Start()
    {
        swipeRecognizer = new TKSwipeRecognizer();

        swipeRecognizer.gestureRecognizedEvent += (r) =>
        {

            GoTweenConfig movePanel = new GoTweenConfig();
            switch (r.completedSwipeDirection.ToString())
            { 
                case "Left":
                    movePanel.localPosition(new Vector3(-Camera.main.pixelWidth, 0, 0));
                    move = true;
                    break;
                case "Right":
                    movePanel.localPosition(new Vector3(Camera.main.pixelWidth, 0, 0));
                    move = true;
                    break;
                default:
                    break;
            }
            movePanel.onComplete(t => {
                if (!move)
                    return;

                    GameObject.FindGameObjectWithTag("GameController").GetComponent<GameLogic>().StartGame();
                    TouchKit.removeGestureRecognizer(swipeRecognizer);
            });
                Go.to(transform, 0.5f, movePanel);
        };

        TouchKit.addGestureRecognizer(swipeRecognizer);
    }
	

}
